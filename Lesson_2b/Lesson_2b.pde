/// user parameters ////
color smileyColor = #00C0FF, // light Blue
      bgColor     = #773E8E, // lavender
      red         = #FF0000,
      green       = #00FF00,
      blue        = #0000FF;
      
final int windowWidth =  800,
          windowHeight = 600;
boolean started = false;

App app;

void settings(){
  size(windowWidth,windowHeight);
}

void setup(){  
  background(bgColor);  
}

void startUpMessage(){
  String s= "Mouse to start/stop movement\nType 'r' to reset\nType 'x' or 'q'  to exit\nType anything else to invert smile\nMouse click to begin...";
  pushStyle();
  pushMatrix();
  stroke(255);
  fill(255);
  translate(width/2,height/2);
  textAlign(CENTER,CENTER);
  textSize(40);
  text(s,0,0);
  popMatrix();
  popStyle();
}

void draw(){
  if (!started){
    startUpMessage();
  }
  else{
    app.draw();
  }
}

void mousePressed() {
  if (!started){
    started = true;
    app = new App();
  }
  else{
    app.toggleMove();
  }
}

void keyPressed() { 
  if (key =='x' || key == 'X' || key =='q' || key == 'Q'){
    exit();
  }
  else  if (!started){
    return;
  }
  else if (key == 'r'){
    app=new App();
  }
  else{
    app.toggleHappiness();
  }
}
