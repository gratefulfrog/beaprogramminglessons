class Hair{
  color hairColor;
  
  
  float rad ,
        epsilon = 15,
        radEps, //= rad+epsilon,
        rightStartAngle = radians(-100),
        leftStartAngle  = radians(120);
  
  float x0  = 0,
        y0  = 0,
        x1, //  = radEps*2,
        y1  = -30,
        y12 = 0,
        x2  = 0,
        y2, //  = radEps,
        x3,//  = radEps,
        y3; //  = radEps;
  
  void setValues(){
    radEps = rad+epsilon;
    x1 = radEps*2;
    y2 = radEps;
    x3 = radEps;
    y3 = radEps;
  }
    
  
  Hair(float faceRadius, color c){
    hairColor = c;
    rad = faceRadius;
    setValues();
  }
  void hairThread(){
    pushStyle();
    pushMatrix();
    stroke(hairColor);
    noFill();
    /*
    stroke(255, 102, 0);
    line(x0,y0,x1,y1);
    line(x2,y2,x3,y3);
    stroke(0, 0, 0);
    */
    bezier(x0,y0,x1,y1,x3,y3,x2,y2);
    translate(x2,y2);
    float coef = 0.6;
    /*
    stroke(blue);
    line(x0,y0,-x1*coef,y12*coef);
    line(x2,y2*coef,-x3*coef,y3*coef);
    //stroke(0, 0, 0);
    */
    bezier(x0,y0,-x1*coef,y12*coef,-x3*coef,y3*coef,x2,y2*coef);
    popMatrix();
    popStyle();
  }  
  
  void hairSide(){
    for (int i=0; i< 20;i++){
      rotate(radians(2));
      hairThread(); 
    }
  }
  void draw(){
    pushMatrix();
    rotate(rightStartAngle);
    hairSide();
    popMatrix();
    pushMatrix();    
    rotate(-rightStartAngle);
    scale(-1,1);  // mirror
    hairSide();
    popMatrix();
  }
}
