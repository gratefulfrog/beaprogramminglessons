class App{
 int incX = 4, // pixels X per frame
    incY = 4; // pixels Y per frame

  final float incLimit         = 20,    // step shift at frame crossing
              jitterAngleLimit = 15,  // degrees
              smileyDiaDiviser = 4;

  boolean move = false;

  //// END OF USER PARAMETERS //////

  float posX,
        posY,
        smileyDia;
  int crashFrameX = 0,
      crashFrameY = 0;
      
  final int crashSkip = 5;    
  
  Smiley s; 
  
  App(){
    background(bgColor);
    fill(0);
    stroke(255);

    smileyDia = height/smileyDiaDiviser;
    s = new Smiley(smileyDia, 0, smileyColor);
    posX = width/2;
    posY = height/2;
  }

  void draw(){
    background(bgColor);
    if (move){
      incrementX();
      incrementY();
    }pushMatrix();
    translate(posX,posY);
    if (move){
      rotate(random(-radians(jitterAngleLimit),radians(jitterAngleLimit)));
    }
    s.draw();
    popMatrix();
    //println(frameCount,posX,posY);
  }

  float getRand(){
    return random(-incLimit,+incLimit);
  }
  
  void incrementX(){
    if (posX >= width-smileyDia/2 || posX <= smileyDia/2){
      if (frameCount - crashFrameX >crashSkip){
        crashFrameX = frameCount;
        incX = -1 * incX;
        posY=max(0,min(height,posY+getRand()));
        //println("X crash", frameCount,posX,posY);
      }
    }
    posX = posX + incX;
  }
  
  void incrementY(){
    if (posY >= height-smileyDia/2 || posY <= smileyDia/2){
      if (frameCount - crashFrameY > crashSkip){
        crashFrameY = frameCount;
        incY = -1 * incY;
        posX=max(0,min(width,posX+getRand()));
        //println("Y crash",frameCount,posX,posY);
      }
    }
    posY = posY + incY;
  }
  
  void toggleMove(){
    move = !move;
  }
  
  void toggleHappiness(){
    s.toggleHappiness();
  }
}
