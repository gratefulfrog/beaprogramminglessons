/// user parameters ////
color smileyColor = #00C0FF, // light Blue
      bgColor     = #773E8E; // lavender
      
final int windowWidth =  600,
          windowHeight = 400;

int incX = 4, // pixels X per frame
    incY = 4; // pixels Y per frame

final float incLimit         = 20,    // step shift at frame crossing
            jitterAngleLimit = 15,  // degrees
            smileyDiaDiviser = 4;

final int crashSkip = 5;

//// END OF USER PARAMETERS //////

// global variables //

float posX,
      posY,
      smileyDia;
int crashFrameX = 0,
    crashFrameY = 0;
    
Smiley s;

void settings(){
  size(windowWidth,windowHeight);
  //fullScreen();
}

void setup(){  
  background(bgColor);
  fill(0);
  stroke(255);
  randomSeed(1);
  //myCirc(posX,posY); 
  smileyDia = height/smileyDiaDiviser;
  s = new Smiley(smileyDia, 0, smileyColor);
  posX = width/2;
  posY = height/2;
}

void draw(){
  //background(bgColor);
  incrementX();
  incrementY();
  pushMatrix();
  translate(posX,posY);
  rotate(random(-radians(jitterAngleLimit),radians(jitterAngleLimit)));
  s.draw();
  popMatrix();
  //println(frameCount,posX,posY);
}

void mousePressed() {
  if (mouseButton==LEFT){
    noLoop();
  }
  else {
    loop();
  }
}

void myCirc(float x, float y){
  ellipse(x,y,50,50);
}

float getRand(){
  return random(-incLimit,+incLimit);
}

void incrementX(){
  if (posX >= width-smileyDia/2 || posX <= smileyDia/2){
    if (frameCount - crashFrameX >crashSkip){
      crashFrameX = frameCount;
      incX = -1 * incX;
      posY=max(0,min(height,posY+getRand()));
      println("X crash", frameCount,posX,posY);
    }
  }
  posX = posX + incX;
}

void incrementY(){
  if (posY >= height-smileyDia/2 || posY <= smileyDia/2){
    if (frameCount - crashFrameY > crashSkip){
      crashFrameY = frameCount;
      incY = -1 * incY;
      posX=max(0,min(width,posX+getRand()));
      println("Y crash",frameCount,posX,posY);
    }
  }
  posY = posY + incY;
}

void keyPressed() {
  if (key == 'h') {
    s.setHappiness(true);
  } else if (key == 's'){
    s.setHappiness(false);
  }
}
