// what is a smiley?
// a circle for the contour
// an arc for the smile
// little filled circles for the eyes

class Smiley {
  float faceD,
        eyeD;
  color strokeColor,
        fillColor;
   boolean happy = true;
   
   Smiley(float diameter, color outlineColor, color insideColor){
     faceD  =  diameter;
     eyeD   = faceD/6.;
     strokeColor = outlineColor;
     fillColor   = insideColor;
   }
   
   void setHappiness(boolean newHappiness){
     happy = newHappiness;
   }
   
   void drawContour(){
     pushStyle();
     fill(fillColor);
     ellipse(0,0,faceD,faceD);
     popStyle();
   }
   void drawEyes(){
     pushMatrix();
     pushStyle();
     translate(-faceD/4,-faceD/5);
     fill(0);  // eyes are black
     ellipse(0,0,eyeD,eyeD);
     translate(faceD/2,0);
     ellipse(0,0,eyeD,eyeD);
     popStyle();     popMatrix();
   }
   
   void drawSmile(){
     pushMatrix();
     pushStyle();
     translate(0,faceD/4);
     noFill();
     strokeWeight(4);
     if (!happy){
       rotate(radians(180));
     }
     arc(0,0,faceD/2,faceD/3,0,PI,OPEN); 
     popStyle();
     popMatrix();
   }     
   
   void draw(){
     pushStyle();
     stroke(strokeColor);
     ellipseMode(CENTER);
     drawContour();
     drawEyes();
     drawSmile();
     popStyle();
   }
}

     
