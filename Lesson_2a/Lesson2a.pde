color smileyColor = #00C0FF, // light Blue
      bgColor     = #773E8E, // lavender
      red         = #FF0000,
      green       = #00FF00,
      blue        = #0000FF,
      hairColor   = blue;


float rad = 50,
      epsilon = 15,
      radEps = rad+epsilon;

float x0  = 0,
      y0  = 0,
      x1  = radEps*2,
      y1  = -30,
      y12 = 0,
      x2  = 0,
      y2  = radEps,
      x3  = radEps,
      y3  = radEps;

void hairThread(float fact){
  pushStyle();
  pushMatrix();
  stroke(hairColor);
  noFill();
  /*
  stroke(255, 102, 0);
  line(x0,y0,x1,y1);
  line(x2,y2,x3,y3);
  stroke(0, 0, 0);
  */
  bezier(x0*fact,y0,x1*fact,y1,x3*fact,y3,x2*fact,y2);
  translate(x2,y2);
  float coef = 0.6;
  /*
  stroke(blue);
  line(x0,y0,-x1*coef,y12*coef);
  line(x2,y2*coef,-x3*coef,y3*coef);
  //stroke(0, 0, 0);
  */
  bezier(x0*fact,y0,-x1*coef*fact,y12*coef,-x3*coef*fact,y3*coef,x2*fact,y2*coef);
  popMatrix();
  popStyle();
  
}  

void hairSide(float fact){
  for (int i=0; i< 20;i++){
    rotate(radians(2));
    hairThread(fact); 
  }
}

void setup(){
  size(800,600);
  translate(width/2,height/2);
  rotate(radians(-100));
  //hairThread(1);
  hairSide(1);
  rotate(radians(120));
  hairSide(-1);
  ellipseMode(CENTER);
  ellipse(0,0,2*rad, 2*rad);
  
}
void draw(){}
