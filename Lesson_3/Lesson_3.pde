// this is lesson 3
// and it is GREAT!

color bgColor = 120,
      red = #FF0000,
      blue = #0000FF,
      green = #00FF00,
      black = 0,
      white = 255;

int direction =1,
    count = 1,
    speed = 1,
    nb=7;
    
boolean rotateEllipses = false;

void setup(){
  size(1000,1000);
  initDisplay();
  stroke(red);
  fill(blue);
}

void draw(){
  pushMatrix();
  translate(width/2,height/2);
  float rotation = radians(count++)*direction*speed;
  rotate(rotation);
  iterEllipse(nb,rotation);
  popMatrix();
  instructions();
}

void initDisplay(){
  background(bgColor);
  instructions();
}

void instructions(){
  final String manual = "'R' to Rotate ellipses - Left Mouse to Accelerate - Right mouse to reset; Speed : ";
  pushStyle();
  fill(bgColor);
  rectMode(CENTER);
  stroke(bgColor);
  rect(width/2, height -20, width,40);
  fill(white);
  stroke(white);
  textSize(24);
  textAlign(CENTER,CENTER);
  text(manual + nf(speed,3),width/2, height -20);
  popStyle();
}

void iterEllipse(int nb, float angle){
  final int rad =  50;
  while (nb-- >0){
    pushMatrix();
    translate(nb*rad,nb*rad);
    if (rotateEllipses){
      rotate(angle);
      angle+=speed;
    }
    ellipse(0,0,rad*1.5,rad);
    popMatrix();
  }
}

void mousePressed(){
  initDisplay();
  if (mouseButton == RIGHT){
    count = speed = direction = 1;
  }
  else{
    direction *=-1;
    count = 1;
    speed++;
  }
}
void keyPressed(){
  if (key == 'r' || key=='R'){
    rotateEllipses = !rotateEllipses;
  }
}
