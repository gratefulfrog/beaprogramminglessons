
float vf_t(float vi, float a, float dt){
  return vi+ a*dt;
}
float vf_d(float vi, float a, float d){
  return sqrt(vi*vi + 2*a*d);
}
float d(float vi, float a, float dt){
  return (a*dt*dt/2 + vi*dt);
}

float lastTime,  // keeping track of time in millis
      dia =  40, // diameter of circle
      radius = dia/2.0,  // radius of circly
      x = radius+1, // initial x
      y = radius+1, // initial y
      vX = 10,       // initial vX
      vY = 0,       // initial
      aX = 0, //4.9,     // x gravitational acceleration
      aY = 9.8,     // y gravitational acceleration
      k  = 0.95,    // coefficient of friction at bounces
      textXOffset = 80,  // text positioning
      textYOffset = 15;  // text positioning

boolean dont = true;

void setup(){
  size(1000,800);
  background(0);
  stroke(255);
  fill(0);
  drawText();
  noLoop(); 
}

void drawText(){
  pushStyle();
  stroke(0);
  rect(width-textXOffset,0,textXOffset,textXOffset);
  fill(255);
  text("vX: " +nfp(vX,2,3)+"\nvY: "+nfp(vY,2,3),width-textXOffset,textYOffset);
  popStyle();
}

void draw(){
  if (dont){
    return;
  }
  float now=millis(),
        dt = (now-lastTime)/100;
  lastTime = now;
  drawText();
  pushMatrix();
  translate(x,y);
  ellipse (0,0,dia,dia);
  
  x += d(vX,aX,dt);
  if (x>=width-radius){
    x = 2*(width-radius)-x;
    vX *=-1*k;
    vY *=k;
  }
  else if (x<= radius){
    x =dia-x;
    vX *=-1*k;
    vY *=k;
  }
  
  y += d(vY,aY,dt);
  if (y>=height-radius){
    y = 2*(height-radius)-y;
    vY *=-1*k;
    vX *=k;
  }
  else if (y<= radius){
    y =dia-y;
    vY *=-1*k;
    vX *= k;
  }
  vX = vf_t(vX,aX,dt);
  vY = vf_t(vY,aY,dt);
  popMatrix();
}
void mousePressed(){
  loop();
  dont=false;
  lastTime= millis(); 
}
  
