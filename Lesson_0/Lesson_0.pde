

void myCirc(float x, float y){
  ellipse(x,y,50,50);
}

float posX =  300,
      posY = 200;
      
int incX = 1, // 7
    incY = 1; // 5

void setup(){
  size(600,400);
  background(#773E8E);
  fill(0);
  stroke(255);
  myCirc(posX,posY); 
}

void incrementX(){
  if (posX >= width || posX <= 0){
    incX = -1 * incX;
  }
  posX = posX + incX;
}

void incrementY(){
  if (posY >= height || posY <= 0){
    incY = -1 * incY;
  }
  posY = posY + incY;
}


void draw(){
  //background(#773E8E);
  incrementX();
  incrementY();
  myCirc(posX,posY); 
}

void mousePressed() {
  if (mouseButton==LEFT){
    noLoop();
  }
  else {
    loop();
  }
}
